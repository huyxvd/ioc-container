﻿using Autofac;

var builder = new ContainerBuilder(); // Khởi tạo ContainerBuilder

// InstancePerDependency || SingleInstance || InstancePerLifetimeScope
builder.RegisterType<A>().As<IA>().InstancePerLifetimeScope(); // 

var container = builder.Build(); // Xây dựng container

// Sử dụng container để resolve dependency
using (var scope = container.BeginLifetimeScope())
{
    var a1 = scope.Resolve<IA>();
    var a2 = scope.Resolve<IA>();
    a1.Hi();
    a2.Hi();
}

using (var scope = container.BeginLifetimeScope())
{
    var a1 = scope.Resolve<IA>();
    var a2 = scope.Resolve<IA>();
    a1.Hi();
    a2.Hi();
}

public interface IA
{
    void Hi();
}

public class A : IA
{
    public A()
    {
        Console.WriteLine($"random {Random.Shared.Next()}");
    }
    public void Hi() => Console.WriteLine("hi");
}
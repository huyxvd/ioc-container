﻿using Autofac;

var builder = new ContainerBuilder(); // Khởi tạo ContainerBuilder

builder.RegisterType<A>().As<IA>(); // Đăng ký dependencies
builder.RegisterType<C>(); // Đăng ký dependencies
//builder.RegisterType<B>().As<IA>(); // thứ tự register

var container = builder.Build(); // Xây dựng container

// Sử dụng container để resolve dependency
using (var scope = container.BeginLifetimeScope())
{
    var c = scope.Resolve<C>();
    c.CallW2Times();
}

public interface IA
{
    void W();
}

public class A : IA
{
    public void W()
    {
        Console.WriteLine("AAAAAAAAAAAAA");
    }
}

public class C
{
    private readonly IA _a;

    public C(IA a)
    {
        _a = a;
    }
    public void CallW2Times()
    {
        _a.W();
        _a.W();
    }
}

//public class B : IA
//{
//    public void W()
//    {
//        Console.WriteLine("BBBBBBBBBBBBBB");
//    }
//}

